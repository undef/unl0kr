Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://gitlab.com/cherrypicker/unl0kr.git
Files-Excluded: lvgl/env_support/cmsis-pack/*
    lvgl/examples/*
    lvgl/demos/*
    lvgl/docs/_static/css/fontawesome.min.css
    lvgl/scripts/built_in_font/SimSun.woff
    *.ttf
Comment: Includes built binaries and minified source not required for Unl0kr

Files: *
Copyright: 2021-2022, Johannes Marbach
           2021, Clayton Craft
           2022, Eugenio Paolantonio (g7)
           2022, Oliver Smith
License: GPL-3+

Files: debian/*
Copyright: 2023, Jarrah Gosbell
License: GPL-3+

Files: lv_drivers/*
       lvgl/*
Copyright: 2019-2021 LittlevGL
License: MIT

Files: lvgl/scripts/built_in_font/FontAwesome5-Solid+Brands+Regular.woff
       FontAwesome5-Solid+Brands+Regular.woff
Copyright: 2012-2018, Dave Gandy <drgandy@alum.mit.edu>
License: OFL-1.1

Files: lvgl/docs/LICENCE.txt
       lvgl/src/misc/lv_printf.c
       lvgl/src/misc/lv_printf.h
Copyright: 2021, LVGL Kft
           Marco Paland <info@paland.com>
           Project Nayuki. (MIT License)
           2006-2021, RT-Thread Development Team
License: MIT

Files: lvgl/env_support/rt-thread/lv_rt_thread_conf.h
       lvgl/env_support/rt-thread/lv_rt_thread_port.c
Copyright: 2006-2021, RT-Thread Development Team
License: MIT

Files: lvgl/env_support/rt-thread/squareline/ui/*
Copyright: 2006-2021, RT-Thread Development Team
License: Apache-2.0

Files: lvgl/src/draw/nxp/*
Copyright: 2020-2022, NXP
License: MIT

Files: lvgl/src/libs/png/lodepng.c
       lvgl/src/libs/png/lodepng.h
Copyright: 2005-2020, Lode Vandevenne
License: BSD-3-clause

Files: lvgl/src/libs/qrcode/qrcodegen.c
       lvgl/src/libs/qrcode/qrcodegen.h
Copyright: Project Nayuki. (MIT License)
License: MIT

Files: lvgl/src/libs/sjpg/tjpgd.c
Copyright: 2021, ChaN
License: MIT

Files: lvgl/src/libs/tiny_ttf/stb_rect_pack.h
Copyright: 2009-2021, Sean Barrett
License: MIT or Public-Domain

Files: lvgl/src/misc/lv_tlsf.h
Copyright: 2006-2016, Matthew Conte
License: BSD-3-clause


License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
      https://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS-IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

License: BSD-3-clause
 Copyright (c) 2019, Advanced Linux Sound Architecture (ALSA) project
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
 .
 3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from
    this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian GNU/Linux systems, the complete text of the GNU General Public
 License version 3 can be found in `/usr/share/common-licenses/GPL-3'.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

License: Public-Domain
 To the extent possible under law, the author(s) have dedicated all copyright
 and related and neighboring rights to this software to the public domain
 worldwide. This software is distributed without any warranty.

License: OFL-1.1
 PREAMBLE
 .
 The goals of the Open Font License (OFL) are
 to stimulate worldwide development of collaborative font projects,
 to support the font creation efforts
 of academic and linguistic communities,
 and to provide a free and open framework
 in which fonts may be shared and improved
 in partnership with others.
 .
 The OFL allows the licensed fonts
 to be used, studied, modified and redistributed freely
 as long as they are not sold by themselves.
 The fonts, including any derivative works,
 can be bundled, embedded, redistributed and/or sold with any software
 provided that any reserved names are not used by derivative works.
 The fonts and derivatives, however, cannot be released
 under any other type of license.
 The requirement for fonts to remain under this license
 does not apply to any document created
 using the fonts or their derivatives.
 .
 DEFINITIONS
 .
 "Font Software" refers to
 the set of files released by the Copyright Holder(s)
 under this license and clearly marked as such.
 This may include source files, build scripts and documentation.
 .
 "Reserved Font Name" refers to
 any names specified as such after the copyright statement(s).
 .
 "Original Version" refers to
 the collection of Font Software components
 as distributed by the Copyright Holder(s).
 .
 "Modified Version" refers to
 any derivative made by adding to, deleting, or substituting --
 in part or in whole --
 any of the components of the Original Version,
 by changing formats
 or by porting the Font Software to a new environment.
 .
 "Author" refers to
 any designer, engineer, programmer, technical writer
 or other person who contributed to the Font Software.
 .
 PERMISSION & CONDITIONS
 .
 Permission is hereby granted, free of charge,
 to any person obtaining a copy of the Font Software,
 to use, study, copy, merge, embed, modify, redistribute,
 and sell modified and unmodified copies of the Font Software,
 subject to the following conditions:
 .
  1) Neither the Font Software nor any of its individual components,
     in Original or Modified Versions,
     may be sold by itself.
  2) Original or Modified Versions of the Font Software
     may be bundled, redistributed and/or sold with any software,
     provided that each copy contains
     the above copyright notice and this license.
     These can be included either as stand-alone text files,
     human-readable headers
     or in the appropriate machine-readable metadata fields
     within text or binary files
     as long as those fields can be easily viewed by the user.
  3) No Modified Version of the Font Software may use
     the Reserved Font Name(s)
     unless explicit written permission is granted
     by the corresponding Copyright Holder.
     This restriction only applies to the primary font name
     as presented to the users.
  4) The name(s) of the Copyright Holder(s)
     or the Author(s) of the Font Software
     shall not be used
     to promote, endorse or advertise any Modified Version,
     except to acknowledge the contribution(s)
     of the Copyright Holder(s) and the Author(s)
     or with their explicit written permission.
  5) The Font Software,
     modified or unmodified, in part or in whole,
     must be distributed entirely under this license,
     and must not be distributed under any other license.
     The requirement for fonts to remain under this license
     does not apply to any document created using the Font Software.
 .
 TERMINATION
 .
 This license becomes null and void
 if any of the above conditions are not met.
 .
 DISCLAIMER
 .
 THE FONT SOFTWARE IS PROVIDED "AS IS",
 WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO
 ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
 AND NONINFRINGEMENT OF COPYRIGHT, PATENT, TRADEMARK, OR OTHER RIGHT.
 IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 INCLUDING ANY GENERAL, SPECIAL, INDIRECT, INCIDENTAL,
 OR CONSEQUENTIAL DAMAGES,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF THE USE OR INABILITY TO USE THE FONT SOFTWARE
 OR FROM OTHER DEALINGS IN THE FONT SOFTWARE.
